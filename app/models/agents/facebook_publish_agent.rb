require 'koala'

module Agents
  class FacebookPublishAgent < Agent
    include FacebookConcern

    cannot_be_scheduled!

    description <<-MD
      The Facebook Publish Agent publishes tweets from the events it receives.
    MD

    def default_options
      {
        'post_type' => 'text', # does ruby really not have enums? wtf
        'post_name' => 'grue',
        'data_path' => '/dev/null'
        'message' => 'yer momz'
      }
    end

    def validate_options
      true
    end

    def working?
      true # of course! 
    end

    def receive(incoming_events)
      incoming_events.each do |event|
        post_text = interpolated(event)['message']
        begin
        case interpolated(event)['post_type']
        when "text"
          facebook.put_connections("me", "feed", { message: post_text } )
        when "video"
          facebook.put_video(data_path, { description: interpolated(event)['post_name'] })
        else  ## placeholder for default
        end
        rescue Koala::Facebook:APIError => e
          create_event :payload => {
            'success' => false,
            'error' => e.message,
            'failed_tweet' => post_text,
            'agent_id' => event.agent_id,
            'event_id' => event.id
          }
        end
      end
    end

  end
end
