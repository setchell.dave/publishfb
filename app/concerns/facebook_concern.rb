require 'koala'

module FacebookConcern
  extend ActiveSupport::Concern
  included do
    validate :validate_facebook_options
  end

  def validate_facebook_options
    unless facebook_app_access_token.present? &&
      facebook_app_id.present? &&
      facebook_access_token.present? &&
      facebook_app_secret.present?
      errors.add('fugly error')
  end

  def facebook_app_access_token
    ENV['FACEBOOK_APP_ACCESS_TOKEN']
  end

  def facebook_app_id
    ENV['FACEBOOK_APP_ID']
  end

  def facebook_access_token
    ENV['FACEBOOK_ACCESS_TOKEN']
  end

  def facebook_app_secret
    ENV['APP_SECRET']
  end

  def facebook
    Koala.configure do |config|
      config.access_token = facebook_access_token
      config.app_access_token = facebook_app_access_token
      config.app_id = facebook_app_id
      config.app_secret = facebook_app_secret
    end
    Koala::Facebook.API.new()
  end
end

